-module(rudy).
-export([start/1, stop/0]).

start(Port) ->
    register(rudy, spawn(fun() -> init(Port) end)).

stop() ->
    exit(whereis(rudy), "time to die").

init(Port) ->
    Opt = [list, {active, false}, {reuseaddr, true}],
    case gen_tcp:listen(Port, Opt) of
        {ok, Listen} ->
            handler(Listen), %% Código completado
            gen_tcp:close(Listen),
            ok;
        {error, Error} ->
            io:format("rudy: init error: ~w~n", [Error])
    end.

handler(Listen) ->
    case gen_tcp:accept(Listen) of
        {ok, Client} ->
            spawn(fun() -> request(Client) end), %% Código completado
            ok;
        {error, Error} ->
            io:format("rudy: handler error: ~w~n", [Error])
    end,
    handler(Listen).

request(Client) ->
    Recv = gen_tcp:recv(Client, 0),
    case Recv of
        {ok, Request} ->
            Response = reply(http:parse_request(Request)), %% Código completado
            gen_tcp:send(Client, Response);
        {error, Error} ->
            io:format("rudy: request error: ~w~n", [Error])
    end,
    gen_tcp:close(Client).

reply({{get, URI, _}, _, _}) ->
    timer:sleep(40), % Delay para simular procesamiento en servidor
    http:ok("ruta: " ++ URI). %% Código completado