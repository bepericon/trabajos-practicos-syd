# Rudy

Un pequeño servidor web que maneja peticiones del metodo GET del protocolo HTTP.

[Enunciado](https://gitlab.com/sistemas_distribuidos/curso/-/blob/master/labs/04-rudy.md)

### Servidor

Para iniciar el servidor es necesario cargar los siguientes módulos:

```
eshell> c(http).
{ok,http}
eshell> c(rudy).
{ok,rudy}
```

A continuación, ejecutar la función start() para exponer el servidor:

```
eshell> rudy:start(8080).
true
```
*Nota: puede ser en el puerto que se nos ocurra, mientras este disponible.*

De esta manera el servidor se encuentra funcionando y vamos a poder accederlo desde el navegador en la url *http://localhost:8080*.
Al acceder a una ruta distinta del servidor, se mostrará en pantalla la URI solicitada.

### Test

Para testear el servidor es necesario 	cargar previamente el siguiente módulo:

```
eshell> c(test). 
{ok,test}
```

Iniciamos las pruebas de tiempos de respuesta de las request al servidor Rudy.
Para utilizar el modulo test utilizamos la función *bench(Host, Port, Requests)*, desde la misma maquina:

```
eshell> test:bench('localhost', 8080, 50).
Para 50 request, tarda: 2.344 segundos
ok
```

Nota: Para testear el servidor desde otra máquina cambiamos el parámetro *Host* por la dirección ip de la maquina donde corre el server.

```
eshell> test:bench('ip_maquina_server', 8080, 50).
Para 50 request,  tarda: 3.462455 segundos
ok
```

## Reportes / Conclusiones
---

- ¿Cuántos request por segundo podemos servir?

Teniendo en cuenta que tenemos el delay de 40 milisegundos.
El servidor atiende **22 request tardando entre 1.00 y 1.05 segundos**, desde la misma maquina.
El servidor atiende **11 request tardando entre 1.00 y 1.05 segundos**, desde otra maquina.

- ¿Nuestro delay artificial es importante o desaparece dentro del overhead de parsing?

Se nota una diferencia bastante grande sacando el delay artificial.
El servidor atiende **900 request tardando entre 1.00 y 1.05 segundos**, desde la misma maquina.
El servidor atiende **60 request tardando entre 1.00 y 1.1 segundos**, desde otra maquina.  
Notamos que el delay artificial, en un servidor que procesa las request en un único thread, ralentiza las respuestas masivas. Esto se debe a que el procesamiento de una request entrante necesita de la devolución de la response de la request anterior, por lo que ante las peticiones masivas, se encolan las solicitudes.

- ¿Qué ocurre si ejecutamos los benchmarks en varias máquinas al mismo tiempo?

Al hacer lo en varias maquinas (la del servidor y una mas), el tiempo en procesar X cantidad de request se termina igualando al tiempo que se tarda en la maquina que no tiene el server. Hay alguna pequeña diferencia pero es de menos de un segundo, como en el siguiente ejemplo, el servidor atiende **100 request tardando entre 8.50 y 9.05 segundos**, desde la misma maquina, mientras desde otra maquina, el servidor atiende **100 request tardando entre 9.50 y 9.90 segundos**.  
Estas pruebas son teniendo en cuenta el delay artificial, en cambio al no tener el delay los tiempos son iguales a los hechos en la prueba anterior para la misma maquina y otra maquina sin correr al mismo tiempo, **900 request y 60 request por segundo**, respectivamente.

## Un poco más allá

### Incrementando el rendimiento

Así como está nuestro server, esperará por un request, la responderá, y esperará al siguiente. Si para servir el request dependemos de otros procesos, como un sistema de archivos o alguna base de datos, el server estará ocioso mientras otro request puede estar listo para ser parseado.

- ¿Deberíamos crear un nuevo proceso por cada request de entrada?

Sí deberiamos crear un nuevo proceso para cada request entrante.
La idea de procesar concurrentemente las request está destinada a aprovechar los "tiempos muertos" y no encolar request que podrían estar siendo procesadas.

- ¿Toma tiempo crear un nuevo proceso?

Crear un nuevo proceso en Erlang toma un tiempo mínimo en comparación a otros lenguajes. La solución de delegar las request entrantes en distintos procesos es aún mejor aprovechada en lenguajes como Erlang en donde la creación de procesos no es costosa.

- ¿Qué ocurriría si tenemos miles de requests por minuto?

Si tenemos miles de request por minuto tendríamos requests procesandose en threads distintos. Vale aclarar que este enfoque es ventajoso más aún si se ejecutara Erlang con soporte para multiprocesador.
Por otra parte, implementar un pool de handlers finito nos permitiría asegurar recursos restringiendo la cantidad de procesos a crear para cada request. De esta forma,en caso de recibir miles de request, la cantidad de procesos sería correspondiente a la cantidad de handlers y las request restantes se encolarían para ser procesadas una vez se liberen los handlers.

### Parseo de HTTP

- ¿Cómo sabremos el tamaño del cuerpo de una request que llega dividida en varios bloques?

El tamaño del cuerpo de la request está descripta en el campo Content-Length del header.
Por ejemplo podríamos recibir una request con la siguiente información:
```
"GET /index.html HTTP/1.1\r\nAccept: application/json\r\nContent-Length: 5\r\n\r\nHello"
```

De esta manera, el tamaño del cuerpo lo conseguiríamos capturando el value de la key Content-Length. Leeríamos el string restante hasta que el length coincida con el proporcionado.


### Devolver archivos

Para que nuestro servidor http permita devolver archivos deberíamos capturar el path y el nombre del archivo desde la URI y brindar el archivo binario mediante el módulo FTP.
El módulo ftp propio de Erlang implementa un cliente para la transferencia de archivos de acuerdo con un subconjunto del Protocolo de transferencia de archivos FTP.

### Robustez

Actualmente la manera en que se detiene el server es "violenta". Se lo puede hacer mas robusto agregando un proceso que actúe de controlador del mismo, de manera que sea el intermediario entre quien solicita detener el server y el server propiamente dicho, donde quiera que se aloje (actualmente se accede de manera local). El server resultante entonces podría recibir mensajes desde el controlador o desde el cliente que envía su request.

Creemos que la forma de implementarlo sería similar al ejercicio Ping Pong de la práctica 1 en donde un proceso envía un mensaje con su propio id para que el proceso receptor tenga la referencia al emisor.
En este caso imaginamos un proceso "controlador" que envía un mensaje de inicio del servidor al proceso "server" para que este último, en caso de necesitarlo, delegue las acciones en el controlador mediante la referencia que se le brindó al inicio y para que el controlador consulte sobre el estado del server.
Esta implementación permitiría que al querer detener el servidor, sea el proceso controlador quien le indique al server a través de mensajes los pasos a seguir antes de detenerlo en su totalidad.