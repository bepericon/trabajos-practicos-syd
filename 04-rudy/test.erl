-module(test).
-export([bench/3]).

% Actualmente espera a obtener la response para generar la siguiente request.
% Para testear la concurrencia, se deberian spawnear las request y hacer tantos receives como request se hicieron.
% Recién después de ejecutar todos los receives se debe tomar el Finish.
% Sino estaríamos testeando cuanto tarda en spawnear la funcion solamente.

bench(Host, Port, Requests) ->
    Start = erlang:system_time(micro_seconds),
    run(Requests, Host, Port),
    Finish = erlang:system_time(micro_seconds),
    io:format("Para ~w request, tarda: ~w segundos ~n", [Requests, (Finish - Start)/1000000]).

run(N, Host, Port) ->
    if
        N == 0 ->
            ok;
        true ->
            request(Host, Port),
            % spawn(fun() -> request(Host, Port) end),
            run(N-1, Host, Port)
    end.

request(Host, Port) ->
    Opt = [list, {active, false}, {reuseaddr, true}],
    {ok, Server} = gen_tcp:connect(Host, Port, Opt),
    gen_tcp:send(Server, http:get("foo")),
    Recv = gen_tcp:recv(Server, 0),
    case Recv of
        {ok, _} ->
            ok;
        {error, Error} ->
            io:format("test: error: ~w~n", [Error])
    end,
    gen_tcp:close(Server).
