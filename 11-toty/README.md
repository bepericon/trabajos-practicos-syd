# Toty

[Enunciado](https://gitlab.com/sistemas_distribuidos/curso/-/blob/master/labs/11-toty.md)

## Inicializando

Compilar los módulos:

```
> rebar3 compile
===> Verifying dependencies...
===> Analyzing applications...
===> Compiling loggy
```

Abrir la consola de rebar3:

```
> rebar3 shell
===> Verifying dependencies...
===> Analyzing applications...
===> Compiling loggy
===> Booted loggy
Eshell V11.2  (abort with ^G)
```
---

## Multicast basico

Este multicast consta de un flujo en donde el *worker* envía un mensaje para que el *multicast* lo comparta con el resto, sin acordar ningún orden.
El resto de los multicast reciben este mensaje y se lo entregan a su worker.

Para iniciar se debe ejecutar la siguiente instrucción:
```
> toty:start(multicast1, 1000, 1000).
```

### Experimento:

- Que pasa si tenemos un sistema que se basa en una entrega con orden total pero esto no fue claramente establecido?

Lo que sucede si no se establece un orden total es que cada worker va a leer los mensajes en un orden propio, sin consensuarlo con los demas workers. Notamos que despues de una cierta cantidad de mensajes recibidos por cada worker estos se empiezan a desordenar, es decir, cada worker lee el mensaje en un orden distinto a los otros.

- Si la congestión es baja y no tenemos retrasos en la red, cuánto tiempo tarda antes de que los mensajes se entreguen fuera de orden?

Ejecutadas algunas pruebas con tiempos de Jitter bajos, notamos que el orden total se rompe igualmente al poco tiempo de empezar la ejecución. Cabe destacar que a partir de ese instante en que se rompe el orden total, los mensajes ya no vuelven a entregarse en el orden correcto.

- Cuán difícil es hacer debug del sistema y darnos cuenta que es lo que está mal?

En este sistema se torna dificultoso el debugging para encontrar fallos debido a la naturaleza de los sistemas distribuidos y además por la complejidad del mismo algoritmo ISIS. Consta de un flujo de mensajes entre procesos muy denso por lo que lo vuelve aún más dificil de observar.
Una forma que nos ayudó a solucionar problemas fue introducir impresiones en consola del estado actual de determinado proceso en los momentos en que envía y recibe mensajes.

## Multicast con Orden Total

Este multicast consta de un flujo en donde el *worker* envía un mensaje para que el *multicast* lo comparta con el resto, acordando un orden en la entrega mediante el algoritmo del sistema ISIS.
Entre todos los multicast se ponen de acuerdo para que se lean los mensajes en el mismo orden. Es decir, para que entreguen los mensajes a su worker en el mismo orden que los demás.

Para iniciar se debe ejecutar la siguiente instrucción:
```
> toty:start(multicast2, 1000, 1000).
```

### Experimentos:

- Mantiene los workers sincronizados? 

Sí se mantienen los workers sincronizados. Una vez implementado el algortimo de ISIS, los workers consensuan un orden total y entregan los mensajes a su worker en el mismo orden.

- Tenemos muchos mensajes en el sistema? cuántos mensajes podemos hacer multicast por segundo y cómo depende esto del número de workers?

Efectivamente tenemos muchos mensajes en la ejecución del sistema; por cada worker que desea enviar un mensaje multicast se realizan distintas idas y vueltas de mensajes para finalmente consensuar un orden. Un envío de mensaje involucra pedir las propuestas enviando mensajes *request* a todos los demás workers, esperar sus propuestas en mensajes *proposal* y finalmente notificarles a estos mismos la propuesta consensuada en mensajes *agreed*.

Después de realizar algunas pruebas, vimos que el consenso del primer mensaje a leer por un worker se realiza recién cuando su multicast tiene ya alrededor de 17 mensajes encolados esperando consenso para ser entregados. Esto va a depender directamente de la cantidad de workers ya que para las request, las propuestas y el consenso, se necesita de enviarle mensajes a todos.

- Construir una red distribuida, cuán grande puede ser antes de que empiece a fallar?

Notamos que no es necesaria una cantidad de workers muy alta para que el sistema comience a fallar. Teniendo en cuenta retardos, congestión por la cantidad de mensajes, creemos que podría llegar a fallar muy pronto, por lo mismo comentado antes, la mensajeria entre procesos crece bastante rapido a medida que se van sumando workers y sus correspondientes multicasts.