-module(worker).
-export([start/4]).

-define(rangeLimit, 20).

start(Name, Multicast, Sleep, Manager) ->
    spawn(fun() -> init(Name, Multicast, Sleep, Manager) end).

init(Name, Multicast, Sleep, Manager) ->
    Gui = gui:start(Name),
    {InitialRGB, _ } = registerMe(Manager),
    Gui ! {set, InitialRGB},
    loop(Name, Multicast, Sleep, Gui, InitialRGB, Manager),
    Gui ! stop.

registerMe(Manager) ->
    Manager ! {register, self()},
    receive
        {register, State, Workers} ->
            {State, Workers}
    end.

loop(Name, Multicast, Sleep, Gui, RGB, Manager) ->
    Wait = rand:uniform(Sleep),
    receive
        {received, Msg} -> % Solo para multicast1
            % io:format("~s recibe el mensaje ~w ~n", [Name, Msg]),
            Manager ! { received, Multicast, Msg},
            NewRGB = updateState(RGB, Msg),
            Gui ! {set, NewRGB},
            loop(Name, Multicast, Sleep, Gui, NewRGB, Manager);
        {received, Msg, PidMulticast} ->
            case erlang:process_info(PidMulticast, registered_name) of 
                {registered_name, NameFrom} -> 
                    Manager ! { received, Multicast, Msg};
                    %io:format("~s recibe el mensaje ~w de ~w ~n", [Name, Msg, NameFrom]);
                [] -> io:format("empty ~n");
                undefined -> io:format("undefined ~n")
            end,
            NewRGB = updateState(RGB, Msg),
            Gui ! {set, NewRGB},
            loop(Name, Multicast, Sleep, Gui, NewRGB, Manager);
        stop ->
            Multicast ! stop,
            ok_worker
    after Wait ->
        N = rand:uniform(?rangeLimit),
        Multicast ! {send, N},
        loop(Name, Multicast, Sleep, Gui, RGB, Manager)
    end.

updateState({R, G, B}, N) ->
    NewR = G,
    NewG = B,
    NewB = (R + N) rem 256,
    {NewR, NewG, NewB}.