-module(toty).

-export([start/3, start/4, stop/0]).

start(Multicast, Sleep, Jitter) ->
    start_(Multicast, Sleep, Jitter),
    ok.

start(Multicast, Sleep, Jitter, TimeTest) ->
    start_(Multicast, Sleep, Jitter),
    wait(TimeTest),
    ok.

start_(Multicast, Sleep, Jitter) ->
    M1 = apply(Multicast, start, []),
    M2 = apply(Multicast, start, []),
    M3 = apply(Multicast, start, []),
    M4 = apply(Multicast, start, []),
    register(john, M1),
    register(ringo, M2),
    register(paul, M3),
    register(george, M4),
    M1 ! {peers, w1, [M1, M2, M3, M4], Jitter},
    M2 ! {peers, w2, [M1, M2, M3, M4], Jitter},
    M3 ! {peers, w3, [M1, M2, M3, M4], Jitter},
    M4 ! {peers, w4, [M1, M2, M3, M4], Jitter},
    register(manager, manager:start([M1, M2, M3, M4])),
    register(w1, worker:start("John", M1, Sleep, manager)),
    register(w2, worker:start("Ringo", M2, Sleep, manager)),
    register(w3, worker:start("Paul", M3, Sleep, manager)),
    register(w4, worker:start("George", M4, Sleep, manager)).

wait(TimeTest) ->
    receive
    after TimeTest ->
        stop()
    end.

stop() ->
    stop(w1), stop(w2), stop(w3), stop(w4), stop(manager).

stop(Name) ->
    case whereis(Name) of
        undefined ->
            ok;
        Pid ->
            Pid ! stop
    end.