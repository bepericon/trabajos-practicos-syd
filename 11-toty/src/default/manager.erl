-module(manager).

-export([start/1]).

start(Multicasts) ->
    Tuplas = lists:map(fun(Multicast) -> {Multicast, []} end, Multicasts),
    spawn(fun() -> init(Multicasts, Tuplas) end).

init(Multicasts, Tuplas) ->
    loop(Multicasts, Tuplas).

loop(Multicasts, Tuplas) ->
    receive
        {register, From} ->
            % Se envia el estado inicial (rgb inicial) y los pids de los demas Multicasts
            From ! {register, {0, 0, 0}, lists:delete(From, Multicasts)},
            loop(Multicasts, Tuplas);
        { received , PidMulticast, Msg} ->
            {value, {Multicast, List}} = lists:keysearch(PidMulticast, 1, Tuplas),
            Tuplas2 = lists:keyreplace(PidMulticast, 1, Tuplas, { Multicast, lists:append(List, [Msg]) }),
            loop(Multicasts, Tuplas2);
        stop ->
            lists:map(fun({Multicast, ListMsg}) -> io:format("Multicast: ~w | Mensajes: ~w ~n",[Multicast, ListMsg]) end, Tuplas)
    end.