-module(ubar).
-export([start/2, request/1, stop/2]).

-define(passengerLocation, {"UNQ", {-34.706096780647556, -58.278551388361606}}).
-define(vehicleLocations, [
    {"Estacion Ezpeleta", {-34.751908762350794, -58.23450251334221}},
    {"Estacion Bernal", {-34.709458349950424, -58.28044451123465}},
    {"Estacion Don Bosco", {-34.70328913965089, -58.29628099709939}},
    {"Estacion Quilmes", {-34.72426738533322, -58.260867531167065}},
    {"Estacion Wilde", {-34.69766795727071, -58.31106065433037}},
    {"Triangulo de Bernal", {-34.71019325322805, -58.3103624668699}},
    {"Municipalidad de Quilmes", {-34.713227090933074, -58.25589065406126}},
    {"Parque de la Cerveceria Quilmes", {-34.73707258377, -58.25867226845901}},
    {"Estadio Centenario", {-34.741551027810246, -58.25207252329546}}
]).

start(Vehicles, Port) ->
    NameMap = spawn_map(),
    Multicasts = spawn_multicasts(Vehicles),
    NameVehicles = generate_name_vehicles(Vehicles),
    set_multicasts(NameVehicles, Multicasts),
    spawn_vehicles(NameVehicles, Multicasts, NameMap),
    NamePassenger = spawn_passenger(NameVehicles, NameMap),
    rudy:start(Port),
    {NamePassenger, NameVehicles}.

request(Destination) ->
    passenger:request(passenger_01, Destination).    

spawn_map() ->
    map:start(map),
    map.

% Se spawnean N cantidad de procesos Multicast
spawn_multicasts(N) ->
    lists:map(fun(_) -> multicast:start() end, lists:seq(1, N)).

% Se generan los nombres para registrar a los N vehiculos
generate_name_vehicles(N) ->
    [list_to_atom(lists:flatten(io_lib:format("vehicle_~w", [X]))) || X <- lists:seq(1, N)].

% Se le envían sus multicasts pares a cada Multicast
set_multicasts(NameVehicles, Multicasts) ->
    NameVehicles_Multicasts = lists:zip(NameVehicles, Multicasts),
    lists:foreach(fun({NameVehicle, Multicast}) -> Multicast ! {peers, NameVehicle, Multicasts} end, NameVehicles_Multicasts).

% Se spawnean los vehiculos
spawn_vehicles(NameVehicles, Multicasts, NameMap) ->
    NameVehicles_Multicasts = lists:zip3(NameVehicles, Multicasts, lists:seq(1, length(NameVehicles))),
    lists:foreach(fun({NameVehicle, Multicast, Index}) -> vehicle:start(NameVehicle, Multicast, NameMap, lists:nth(Index, ?vehicleLocations)) end, NameVehicles_Multicasts).

% Se spawnea usuario
spawn_passenger(NameVehicles, NameMap) ->
    passenger:start(passenger_01, NameVehicles, NameMap, ?passengerLocation),
    passenger_01.

stop(NamePassenger, NameVehicles) ->
    passenger:stop(NamePassenger),
    lists:foreach(fun(NameVehicle) -> stop_(NameVehicle) end, NameVehicles),
    rudy:stop().

stop_(Name) ->
    case whereis(Name) of
        undefined ->
            ok;
        Pid ->
            Pid ! stop
    end.