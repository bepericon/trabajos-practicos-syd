-module(passenger).
-export([start/4, stop/1, request/2]).

-define(waitTime, 10000).

start(Name, Vehicles, Map, Location) ->
    Passenger = spawn(fun() -> init(Name, Vehicles, Map, Location) end),
    register(Name, Passenger).

stop(Name) ->
    Name ! stop.

request(Passenger, Destination) ->
    Passenger ! { request, Destination }.

init(Name, Vehicles, Map, {Location, Coordinates}) ->
    map:spawn(Map, Name, {Location, Coordinates}),
    normal(Name, Vehicles, Map).

normal(Name, Vehicles, Map) ->
    io:format("~w disponible para pedir viaje. ~n", [Name]),
    receive
        { request, {Destination, Coordinates} } ->
            if 
                Vehicles == [] ->
                    io:format("No hay vehiculos. ~n", []),
                    normal(Name, Vehicles, Map);
                true ->
                    io:format("~w pide un vehiculo para dirigirse a ~s. ~n", [Name, Destination]),
                    search(Name, Vehicles, Map, {Destination, Coordinates})
            end;
        stop ->
            io:format("~w se deconecta ~n", [Name]),
            ok
    end.

search(Name, Vehicles, Map, Destination) ->
    Location = map:location(Map, self(), Name),
    requests(Name, Vehicles, Location, Destination),
    receive
       { accept, NameVehicle, {VehicleLocation, _} } ->
           io:format("~w ubicado en ~s acepta el viaje ~n", [NameVehicle, VehicleLocation]),
           VehicleMonitor = monitor(process, NameVehicle),
           wait(Name, Vehicles, Map, NameVehicle, Destination, VehicleMonitor)
    after ?waitTime ->
        io:format("~w cancela el pedido de viaje por tiempo excedido de espera ~n", [Name]),
        normal(Name, Vehicles, Map)
    end.

requests(Name, Vehicles, Location, Destination) ->
    lists:foreach(fun(NameVehicle) -> vehicle:request(NameVehicle, Name, Location, Destination) end, Vehicles).

wait(Name, Vehicles, Map, NameVehicle, Destination, VehicleMonitor) ->
    io:format("~w esperando vehiculo ~w ~n", [Name, NameVehicle]),
    receive
        arrives ->
            io:format("~w llega a recojer al pasajero ~w ~n", [NameVehicle, Name]),
            travel(Name, Vehicles, Map, NameVehicle, Destination);
        {'DOWN', VehicleMonitor, process, Object, _} ->
            io:format("~w tuvo un inconveniente. Se cancela el viaje. ~n", [Object]),
            Vehicles2 = lists:delete(NameVehicle, Vehicles),
            search(Name, Vehicles2, Map, Destination)
    end.

travel(Name, Vehicles, Map, NameVehicle, Destination) ->
    NameVehicle ! board,
    receive
        reached ->
            io:format("~w llega a su destino ~n", [Name]),
            map:update(Map, Name, Destination),
            normal(Name, Vehicles, Map)
    end.