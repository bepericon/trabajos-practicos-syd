-module(map).
-export([start/1, stop/1, spawn/3, update/3, location/3, getLocationsString/2, generateString/1]).

-define(averageVelocity, 30).

start(Name) ->
    Map = spawn(fun() -> init() end),
    register(Name, Map).

stop(Name) ->
    Name ! stop.

spawn(Name, Member, Location) ->
    Name ! { spawn, Member, Location }.

update(Name, Member, Location) ->
    Name ! { update, Member, Location }.

location(Name, From, Member) ->
    Name ! { location, From, Member },
    receive
        { location, Location } -> Location
    end.

getLocationsString(Name, From) ->
    Name ! { front, From },
    receive
        { front, Locations } -> Locations
    end.

init() ->
    loop([]).

loop(Members) ->
    receive
        { spawn, From, Location } ->
            Members2 = setLocation(From, Location, Members),
            loop(Members2);
        { update, From, Location } ->
            Members2 = updateLocation(From, Location, Members),
            loop(Members2);
        { location, From, Member } ->
            Location = getLocation(Member, Members),
            From ! { location, Location },
            loop(Members);
        { time, From, {_, FromCoordinates}, {_, ToCoordinates} } ->
            Time = time(FromCoordinates, ToCoordinates),
            From ! { time, Time },
            loop(Members);
        { front, From } ->
            String = generateString(Members),
            From ! { front, String },
            loop(Members);
        stop ->
            ok
    end.

generateString(Members) ->
    Members2 = lists:map(fun(Member) -> generateObject(Member) end, Members),
    lists:flatten(io_lib:format("~p",[Members2])).

generateObject({ Name, {_, {Latitude, Longitude}}}) ->
    Name2 = atom_to_list(Name),
    "{\"name\":\""++ Name2 ++"\", \"latitude\":\""++ io_lib:format("~.14f", [Latitude]) ++"\", \"longitude\":\""++ io_lib:format("~.14f", [Longitude]) ++"\"}".

setLocation(Member, Location, Members) ->
    [{ Member, Location } | Members].

updateLocation(Member, Location, Members) ->
    lists:keyreplace(Member, 1, Members, {Member, Location}).

getLocation(Member, Members) ->
    case lists:keysearch(Member, 1, Members) of
        {value, { _, Location }} ->
            Location;
        false ->
            io:format("No se pudo obtener la ubicacion de ~w ~n", [Member]),
            error
    end.

% Se obtiene el tiempo en segundos entre las dos ubicaciones
time({Latitude1, Longitude1}, {Latitude2, Longitude2}) ->
    Distance = haversineDistance(Latitude1, Longitude1, Latitude2, Longitude2),
    Velocity = ?averageVelocity,
    Time = (Distance / Velocity) * 36000,
    round(Time).

% Se obtiene la distancia entre las dos ubicaciones
haversineDistance(Lat1, Lng1, Lat2, Lng2) ->
    Deg2rad = fun(Deg) -> math:pi()*Deg/180 end,
    [RLng1, RLat1, RLng2, RLat2] = [Deg2rad(Deg) || Deg <- [Lng1, Lat1, Lng2, Lat2]],
 
    DLon = RLng2 - RLng1,
    DLat = RLat2 - RLat1,
 
    A = math:pow(math:sin(DLat/2), 2) + math:cos(RLat1) * math:cos(RLat2) * math:pow(math:sin(DLon/2), 2),
 
    C = 2 * math:asin(math:sqrt(A)),
 
    %% suppose radius of Earth is 6373 km approx.
    Km = 6373 * C,
    Km.