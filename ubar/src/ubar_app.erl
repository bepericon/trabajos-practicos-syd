%%%-------------------------------------------------------------------
%% @doc ubar public API
%% @end
%%%-------------------------------------------------------------------

-module(ubar_app).

-behaviour(application).

-export([start/2, stop/1]).

start(_StartType, _StartArgs) ->
    ubar_sup:start_link().

stop(_State) ->
    ok.

%% internal functions
