# Ub.ar

## Índice

- [Introduccion](#id_introduccion)
- [Alcance propuesto](#id_alcance)
- [Solución del problema](#id_solucion)
- [Robustez](#id_robustez)
- [Trabajo futuro](#id_futuro)
- [Pasado pisado](#id_pasado)
- [Diagramas](#id_diagramas)
- [Inicializando proyecto](#id_inicializando)
- [Presentación (miro.com)](https://miro.com/app/board/o9J_l9VlwOY=/)

---

<div id='id_introduccion'></div>

## Introducción

Para esta entrega pensamos en simular un sistema distribuido basandonos en los servicios de transporte contemporáneos conocidos por su uso en celulares móviles (Cabify, Uber, DiDi, etc). Para implementarlo en Erlang pensamos en una solución en donde dos partes intervienen, **pasajeros y vehículos**.
El flujo principal del sistema es el siguiente:

1. Pasajero pide el vehículo.
2. vehículos compiten por el pedido.
3. Un solo vehículo es el ganador.

El problema que encontramos y tratamos de solucionar es la comunicación entre las dos partes solamente. Cómo estas terminan sabiendo quién es quién y cmo se organizan para saber cumplir su rol de una manera sencilla.  
Nombramos entonces las dos instancias importantes a resolver en la comunicación:
- Por un lado **que un pasajero pida un vehículo** para su viaje y como este despues de cierto tiempo se entera cual es el vehículo.
- Y por otro, **decidir cual es el vehículo asignado** para el pasajero que solicito el viaje.

---

<div id='id_alcance'></div>

## Alcance propuesto

#### **Etapa 1 (alcance principal)**

Flujo donde el pasajero consigue un vehículo (flujo feliz).

1. Pasajero pide el vehículo.
2. vehículos compiten por el pedido.
3. Un solo vehículo es el ganador.


#### **Etapa 2**

Flujo donde el pasajero cancela sin tener todavia un vehículo asignado.

1. Pasajero pide el vehículo.
2. vehículos compiten por el pedido.
3. Cancelacion por parte del pasajero.
4. Los vehículos dejan de competir.

#### **Etapa 3**

Flujo donde el pasajero cancela ya teniendo un vehículo asignado.

1. Pasajero pide el vehículo.
2. vehículos compiten por el pedido.
3. Un solo vehículo es el ganador.
4. Cancelacion por parte del pasajero.

#### **Etapa 4**

Flujo donde el vehículo cancela ya habiendo ganado (tiene un pasajero asignado)

1. Pasajero pide el vehículo.
2. vehículos compiten por el pedido.
3. Un solo vehículo es el ganador.
4. Cancelacion por parte del vehículo.

#### **Etapa 5**

Agregar tolerancia a fallos (robustez).

#### **Etapa opcional**

Exponer una api para agregar interfaz gráfica.

---

<div id='id_solucion'></div>

## Solución del problema

### Arquitectura

Para no caer en un **sistema cliente-servidor** (ver apartado [Pasado pisado](#id_pasado)), optamos por hacer que **cada uno de las partes que intervienen sean un proceso**. Con esta forma particular de los interventores, como bien se dijo antes (ver apartado [Introduccion](#id_introduccion)), surjen dos problemas de comunicación, los cuales abordamos teniendo en cuenta **algoritmos que ya conociamos de comunicación entre procesos** (idea base: [11-toty](https://gitlab.com/Maty11C/trabajos-practicos-syd/-/tree/master/11-toty)). Y también, a la par **estructuramos a los procesos pasajero y vehículo con estados** (idea base: [12-muty](https://gitlab.com/Maty11C/trabajos-practicos-syd/-/tree/master/12-muty)) para poder tomar decisiones en base al estado en el que se encuentren. 
Dividimos la comunicación en **dos capas de multicast**:
- En la primer capa simplemente enviamos un mensaje a todos los **procesos vehículos**, siendo estos conocidos por el **proceso pasajero**, para representar el pedido de un vehículo. Este ultimo queda a la espera de la respuesta.
- Para que entre los **procesos vehículos** se obtenga un ganador, se implementó un **proceso multicast** por cada uno para hacer posible la **segunda capa de multicast**. Los **multicast** se comunican entre sí, y determinan el ganador teniendo en cuenta quien fue el que contesto más rápido.

### Algoritmo

Explicamos más en detalle y lenguaje 'mortal' el algoritmo usado en esta segunda capa, ya que es la más compleja.  
Los **multicast** guardan y envían una **referencia (instante de tiempo)** al resto. Cada uno al recibir las **refencias** de otro, las compara con la **referencia propia** (sabiendo la cantidad de multicast en total), y evalúa el resultado para obtener alguno de los siguientes escenarios:
- **unfinished**: cuando aún no se obtuvo la información necesaria para determinar si es el ganador o el perdedor.
- **loser**: cuando ya se obtuvo la información necesaria y se determinó que la referencia propuesta no es la ganadora (la referencia propuesta no es la de tiempo menor). Se procede a notificar al vehículo de que es el perdedor.
- **winner**: cuando ya se obtuvo la informaciónnecesaria y se determinó que la referencia propuesta es la ganadora (la referencia propuesta es la de tiempo menor). Se procede a notificar al vehículo de que es el ganador.

Al final, "se volveria a la primer capa de multicast". El **proceso vehículo ganador** es el que se comunica con el **proceso pasajero**.  

Vale recalcar, que en cada proceso se va produciendo un **cambio de estado** (ver apartado [Diagramas](#id_diagramas)) dependiendo de en que momento del flujo del sistema se encuentra dicho proceso. De esta manera podemos saber que acciones tomar al respecto dependiendo de cada estado.

---
<div id='id_robustez'></div>

## Robustez

### Ubicación

Para lograr simular la ubicación del pasajero y de los vehículos se incorporaron las **locaciones** a los procesos. Estas constan de un nombre y de las coordenadas (latitud y longitud).  
La implementación y la responsabilidad de "saber" la ubicación de cada proceso sin embargo se delegó en un módulo llamado **map**. Este módulo es quien se encarga de setear, actualizar e informar locaciones y de calcular las distancias y tiempos entre las mismas.

*Nota: se simulan los tiempos de viaje (buscar un pasajero y llevarlo a su destino) con sleeps que correspondan con el tiempo que implicaría llegar de una ubicación a otra.*

### Tolerancia a fallos

A la hora de pensar en que nuestro sistema sea más robusto en la ejecución, notamos que era viable incorporar tolerancia a fallos en la instancia en que el vehículo se encuentra en viaje para recojer al pasajero. La idea de soportar un fallo en ese lapso de tiempo es para simular que el vehículo tiene un inconveniente y que cause la cancelación del viaje y permita al pasajero continuar con el pedido de otro vehículo.

Para implementar lo mencionado incorporamos monitores para lograr que el proceso **pasajero** y el proceso **multicast** correspondiente observen al vehículo desde el momento en que acepta el viaje. De esta forma, en cuanto el proceso **vehículo** se "caiga", el pasajero y el multicast van a ser notificados para que por un lado el pasajero proceda con el pedido de otro vehículo y por el otro el multicast le notifique a sus pares la baja de un vehículo.

*NOTA: Cuando se cae un usuario es mas complicado porq no sabemos de donde sacar el parametro de vehículos.*

### Interfaz gráfica

En conjunto con la **ubicación** y la **Etapa opcional**, se pensó en implementar una aplicación web que visibilice a las partes (pasajero y vehículos) posicionadas en un mapa interactivo con el fin de evitar la lectura de los eventos en la consola.   
La idea fue tener un **frontend web** realizando consultas a una **api expuesta en Erlang**. A su vez, la api consume la información necesaria directamente del módulo **map** sin necesidad de comunicarse con el pasajero y los vehículos.

...

---
<div id='id_futuro'></div>

## Trabajo futuro

Quedaron pendientes varias etapas (ver apartado [Alcance propuesto](#id_alcance)): 
- Etapa 2
- Etapa 3
- Etapa 4

Con respecto a la **ubicación** de los procesos pensamos en que se podrían agregar las siguientes funcionalidades: 
- Restringir la búsqueda a los vehículos más cercanos.
- Establecer una condición extra al decidir el ganador y que no baste solamente con haber contestado primero (se podría agregar prioridad al vehículo que se encuentre más cercano al pasajero).
- Actualizar la ubicación periódicamente (lapsos de tiempos más cortos) y no solamente en el principio y fin de un viaje.

Por otra parte tratamos de implementar una mejora en el algoritmo de competencia para que aquel multicast que identifique que ya no tiene posibilidades de ganar la competencia deje de evaluar las propuestas ("que corte antes"). Esta optimización nos resultó problemática ya que cortar con la evaluación de las propuestas antes de tiempo implicaba seguir recibiendo propuestas innecesarias que afectaban a la ejecución normal. De todas maneras creemos que podría implementarse la mejora.

Así como pensamos en la tolerancia a fallos en la instancia en que el vehículo se dirige a recojer al pasajero, tambíen pensamos en agregarlo en el proceso **pasajero** para que ante la eventual caída del proceso, sea algún intermediario el que se encargue de relanzarlo y evitar que se caiga la aplicación. Después de analizarlo, vimos que era dificultoso ya que además de necesitar los parametros iniciales del proceso pasajero para levantarlo, también necesitabamos mantener el estado en el que se encontraba con sus correspondientes parámetros, por lo que decidimos posponerlo pero plantearlo como una posible mejora.

---
<div id='id_pasado'></div>

## Pasado pisado

Desde un principio pensamos en utilizar algun algoritmo familiar de trabajos anteriores, pero no fue tan facil darnos cuenta como adecuar esas ideas, conocimientos a nuestro sistema.

Cuando empezamos a plantear los estados en los dos modulos, caimos en un modelo cliente-servidor en donde el pasajero, despues de solicitar un vehículo, "actuaba" de servidor. Digamos que era el que recibia todas las respuestas y en base a ellas sabia cual era el vehículo ganador. En fin, nos ayudo a darnos cuenta y avanzar de otra forma.

Al querer basarnos en un algoritmo de consenso para la obtención del vehículo ganador caimos en distintos debates con ninguna salida. Despues de chocarnos bastante con eso nos dimos cuenta que no debian consensuar al ganador, si no enterarse quien lo es, lo cual parece lo mismo pero no. De esta manera pudimos llegar a una forma de resolver la competencia entre los vehículos.

---
<div id='id_diagramas'></div>

## Diagramas

En esta primer imagen vemos el flujo de estados que tenemos en las partes que intervienen en nuestro sistema.

![states](./assets/01-ubar-states.jpg)

En esta imagen siguiente vemos en que lugar del flujo de estados se actualizan las ubicaciones de los participantes.

![states](./assets/02-ubar-ubication.jpg)

En las siguientes imagenes veremos nuestras dos capas de multicast, comunicación entre procesos para saber quien es el ganador.

![states](./assets/03-ubar-multicast.jpg)
![states](./assets/04-ubar-multicast.jpg)
![states](./assets/05-ubar-multicast.jpg)
![states](./assets/06-ubar-multicast.jpg)

---
<div id='id_inicializando'></div>

## Inicializando proyecto

Compilar los módulos:

```
> rebar3 compile
===> Verifying dependencies...
===> Analyzing applications...
===> Compiling ubar
```

Abrir la consola de rebar3:

```
> rebar3 shell
===> Verifying dependencies...
===> Analyzing applications...
===> Compiling ubar
===> Booted ubar
Eshell V11.2  (abort with ^G)
```

Iniciar ubar especificando la cantidad de vehículos a disponibilizar y el puerto:

```
> ubar:start(10, 8080).
```

Pedir un vehículo para un viaje a determinado destino:

```
> Destino = {"Nombre", {Latitud, Longitud}}.
> ubar:request(Destino).
```