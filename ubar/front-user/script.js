var mymap = L.map('mapid').setView([-34.70927308384127, -58.280418432759426], 16);

L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
    maxZoom: 18,
    minZoom: 3
}).addTo(mymap);


fetch('http://localhost:8080/', { method: 'GET'})
    .then(res => res.text())
    .then(data => {
        let {uri, body} = JSON.parse(data);
        console.log(`{ uri: ${uri}, body: [${body}] }`);
        
        body.forEach(member => {
            let {name, latitude, longitude} = JSON.parse(member);
            var marker = L.marker([parseFloat(latitude), parseFloat(longitude)]).addTo(mymap);
            marker.bindPopup(`<b>${name}</b><br><b>{${latitude},${longitude}}</b>`);            
        });
    });