    -module(pong).

    -export([response/0]).

    response() ->
        receive
            {Id_Ping, Msg} -> io:format("Pong recibe: ~p~n", [Msg]),
            Id_Ping ! "Respuesta desde pong!"
        end,
        response().