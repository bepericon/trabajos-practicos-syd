# Muty

[Enunciado](https://gitlab.com/sistemas_distribuidos/curso/-/blob/master/labs/12-muty.md)

## Inicializando

Compilar los módulos:

```
> rebar3 compile
===> Verifying dependencies...
===> Analyzing applications...
===> Compiling muty
```

Abrir la consola de rebar3:

```
> rebar3 shell
===> Verifying dependencies...
===> Analyzing applications...
===> Compiling muty
===> Booted muty
Eshell V11.2  (abort with ^G)
```

## Los locks

En Erlang los mensajes son encolados en el mailbox de los procesos. Si matchean un patrón en la cláusula receive son procesados, pero sino se mantienen en la cola. En nuestra implementación aceptamos y procesamos alegremente todos los mensajes, aunque algunos, como el mensaje request cuando estamos en el estado held, son simplemente guardados para después. La razón por la que lo implementamos de esta manera es para hacer explícito que los mensajes son tratados aún en el estado held.

- ¿Sería posible usar la cola de mensajes Erlang y dejar que los mensajes se encolen hasta que se libere el lock?

Entendemos que la cola de mensajes de Erlang sería una opción que se podría aprovechar ya que el motivo de implementar el pattern matching por estados es simplemente para mostrar que un lock procesa request tanto en estado "wait" como en estado "held", incluso la implementación de la cláusula "release" en el receive es la misma para los dos estados mencionados.
Sin embargo, en nuestra implementación vemos que no seria posible usar la cola de mensajes de Erlang y dejar que los mensajes se encolen hasta que se libere el lock porque, a pesar de que los mensajes se procesarían uno por uno de forma ordenada tal como lo hace la cola "Waiting", se perderían las referencias que los demás locks necesitan matchear para descartar al momento de recibir el "ok" del proceso que liberó el lock. Eliminando la cola "Waiting", el lock que recibe el "ok" no tendría forma de chequear la referencia.

- ¿Por qué no estamos esperando mensajes ok?

El lock en estado "held" no espera mensajes "ok" porque no tiene sentido que los reciba en esa instancia; de hecho si se encuentra en estado "held" significa que ya los esperó y recibió anteriormente cuando se encontraba en estado "wait".
A aquel proceso que se encuentre en estado "held" solo le resta terminar y liberar el lock haciendo release.

## Algo de testing

Utilizando muty:start(Lock, Sleep, Work) empezamos a testear el modulo lock1.

- ¿Funciona bien? ¿Qué ocurre cuando incrementamos el riesgo de un conflicto de lock? ¿Por qué?

Funciona bien, a pesar de ser propenso a deadlocks, porque cumple con el objetivo de garantizar la exclusión mutua.
Entendemos que incrementar el riesgo de un conflicto de lock implica bajar el tiempo de Sleep y subir el de Work. Esto lo corroboramos haciendo algunas pruebas notando que a medida que fue bajando el tiempo de Sleep y subiendo el tiempo de Work, los deadlocks fueron aumentando.
Creemos que los deadlocks incrementan porque un proceso al "dormir" poco, se encuentra más tiempo "compitiendo" por el conseguir el lock. Además, a esto se suma que el proceso que en esos momentos posee el lock no lo libera tan fácilmente ya que lo retiene por un tiempo más prolongado. Estos dos factores atentan contra el tiempo de espera límite del proceso que pide el lock lo que causa que pasados los 4 segundos se obtenga un deadlock.

## Resolviendo el deadlock

El problema con esta primera solución puede ser resuelto si le damos a cada lock un identificador único 1, 2 ,3 y 4. El identificador dará prioridad al lock. Un lock en el estado de espera enviará un mensaje ok a un lock que lo solicite si el lock tiene una prioridad más alta (siendo 1 la prioridad máxima).
Hay una situación que debemos tratar correctamente. Si no, corremos el riesgo de tener dos procesos en la sección crítica al mismo tiempo.
Implementemos esta solución en un módulo llamado lock2, y veamos que funciona aunque tengamos alta contención. Corramos algunos tests y veamos qué tan bien funciona la solución.

- ¿Funciona? ¿Podemos garantizar que tenemos un solo proceso en la sección crítica en todo momento?

Sí funciona y garantiza que un solo proceso ingresa a la sección crítica dado que se tuvo en cuenta la situación mencionada que se debía tratar correctamente.
La situación incorrecta podría haberse dado cuando el proceso al proceso que "le robaron el lock", por tener una prioridad menor, reenvíaba una request al proceso más prioritario pero sin eliminar la referencia a la primer request. Partiendo de este escenario planteado podría suceder que se entre crucen request y los dos procesos ingresen a la sección crítica.

- ¿Qué tan eficientemente lo hace y cuál es la desventaja? Reportar cuáles son los resultados.

La eficiencia en cuanto a la implementación del lock1 disminuye ya que se deben hacer ciertos ajustes como los antes mencionados para solucionar la situación que rompería con la condición de mutex, además de que se implementa un traspaso de lock por prioridad lo que también implica un control agregado. Mas allá de estos cambios notamos que la baja de la eficiencia es casi imperceptible por lo que podría decirse que la eficiencia sigue siendo aceptable.
La desventaja de implementar una prioridad arbitraria en los locks es que los locks con más prioridad van a ser los que más veces accedan a la sección crítica, mientras que los locks de menor prioridad accederán pocas veces o incluso ninguna y producir starvation.

## Tiempo de Lamport

Una mejora es permitir a los locks ser tomados con prioridad dada por el orden temporal. El único problema es que no tenemos acceso a relojes sincronizados (asumiendo que estamos ejecutando en una red asíncrona). La solución es utilizar relojes lógicos como los relojes de Lamport.

Para implementar esto debemos agregar una variable de tiempo al lock. El valor es inicializado a cero pero es actualizado cada vez que el lock recibe un mensaje de request desde otro lock. El reloj de Lamport entonces lleva cuenta del request mas alto que se ha visto hasta el momento. Cuando un request es enviado, deberá tener un timestamp una unidad mas alto del visto hasta el momento.

Ahora vemos una solución donde el timestamp de Lamport no es necesario en todos los mensajes del sistema, sino en aquellos que son importantes, por ejemplo, los mensajes de request.

Cuando un lock está en estado de espera debe determinar si el request fue enviado antes o después de haber enviado su propio mensaje. Si esto no puede determinarse, entonces se utilizará el identificador de lock para resolver el orden.

Notemos que los workers no están involucrados con el reloj de Lamport.

- ¿Puede darse la situación en que un worker no se le da prioridad al lock a pesar de que envió el request a su lock con un tiempo lógico anterior al worker que lo consiguió?

Sí puede darse la situación de que un worker que envió la request con un tiempo lógico menor consiga el lock después de otro que lo pidió con un tiempo lógico mayor porque actualizamos el reloj de Lamport únicamente en los mensajes de request.

## Reporte

### Lock 2

El lock 2 se resolvió apoyándose en la implementación del lock1 añadiendo un identificador a cada uno de los locks para determinar la prioridad ante el pedido de acceso a la sección crítica.
De esta forma, si un lock le envía una request a uno de menor prioridad, por más que este último se encuentre en la espera de ok de sus pares, le responderá con un ok concediendole el acceso a la sección crítica y no lo agregará a su lista de espera. Además, en el caso antes mencionado, el lock de menor prioridad le envía una nueva request al lock de mayor prioridad para constatar de que este último sepa que el lock de menor prioridad sigue queriendo ingresar a la sección crítica.

Los problemas que tuvimos fueron en los ajustes que debíamos hacer para considerar la situación en la que un lock le enviaba una request a otro de menor prioridad que se encontraba en espera. Mas precisamente tuvimos problemas al momento de eliminar la referencia de la request obsoleta que se le había enviado al lock de mayor prioridad.
El principal inconveniente fue poder identificar a la referencia obsoleta a eliminar; para solucionarlo optamos por guardar las referencias en forma de tupla junto al lock correspondiente (lock al que se le envío la request) y no la referencia únicamente.

La conclusión que sacamos es que es una implementación robusta que se ajusta a sistemas distribuidos reales. Entendemos que la problemática del mutex es un escenario válido partiendo de que en los sistemas conviven procesos con distintas prioridades, con algunos más importantes que otros, por lo que resolver el acceso único a la sección crítica mediante un identificador que refleje la prioridad del proceso es una solución aplicable.

El pro de la solución son la organización de los locks para acceder a la sección crítica a través de la prioridad. Creemos que es una solución prolija porque sigue siendo transparente para el worker, es decir, el worker sigue sin saber la forma en que los locks resuelven el mutex.
La contra es que no realiza ningún tipo de control para evitar que se produzca inanición en algunos de los locks. Esto significa que los locks de menor prioridad, pueden nunca ingresar a la sección crítica.

### Lock 3

El lock 3 se resolvió implementando Tiempo de Lamport para permitirle a los locks acceder a la sección crítica dada la prioridad por el orden temporal en que se solicitó el acceso.
La forma de resolverlo fue incorporando el módulo time utilizado en loggy para que sumada a la variable de tiempo de cada lock, implemente el algoritmo de tiempos de Lamport para los mensajes de request.

No tuvimos grandes problemas para implementar el lock3, dado que el algoritmo ya era conocido de loggy, salvo por adaptar el incremento del contador en el momento que el lock envía las request a sus pares.

La conclusión que sacamos es que es una solución un poco más justa al considerar el posible escenario de inanición de algunos locks y aplicable a sistemas que consten de procesos con igual prioridad, o al menos sin procesos prioritarios por sobre otros. Podría decirse que los locks se encuentran en iguales condiciones para "competir" por el acceso a la sección crítica a diferencia del lock2 donde algunos locks corrían con ventaja por sobre otros (los de más prioridad en base a su identificador).

El pro de la solución creemos que sigue siendo la transparencia de la implementación desde el punto de vista del worker y la delegación de lógica de orden de los eventos en el módulo time para implementar el tiempo Lamport. Al igual que en loggy, se plantea una solución que permite que sea factible otra implementación de tiempos.
Y la contra creemos que sigue siendo que no se realiza tampoco ningún tipo de control para evitar inanición de algunos locks; por más remota que sea, sigue existiendo la posibilidad de que algún lock no acceda nunca a la sección crítica.