-module(lock2).

-export([start/1]).

start(Id) -> spawn(fun () -> init(Id) end).

init(Id) ->
    receive
        {peers, Peers} -> open(Id, Peers);
        stop -> ok
    end.

open(Id, Nodes) ->
    receive
        {take, Master} ->
            Refs = requests(Id, Nodes),
            wait(Id, Nodes, Master, Refs, []);
        {request, From, Ref, _} ->
            From ! {ok, self(), Ref},
            open(Id, Nodes);
        stop ->
            ok
    end.

requests(Id, Nodes) ->
    lists:map(fun(P) ->
                      R = make_ref(), 
                      P ! {request, self(), R, Id},
                      {P, R} % {Node, Ref}
              end, Nodes).

wait(Id, Nodes, Master, [], Waiting) ->
        Master ! taken,
        held(Id, Nodes, Waiting);

wait(Id, Nodes, Master, Refs, Waiting) ->
    receive
        {request, From, Ref, FromId} ->
            if
                FromId < Id -> % Si el lock con menor prioridad recibe una request de uno con mayor prioridad (Id mas chico)
                    From ! {ok, self(), Ref}, % El lock de menor prioridad le envía un ok al de mayor prioridad
                    NewRef = make_ref(), % Se crea una nueva referencia para reenviar una request
                    From ! {request, self(), NewRef, Id}, % El lock de menor prioridad le envía una request al de mayor prioridad para que lo tenga en cuenta
                    Refs2 = lists:keydelete(From, 1, Refs), % Se borra la referencia vieja
                    wait(Id, Nodes, Master, [{From, NewRef}|Refs2], Waiting); % Se agrega la referencia nueva
                true ->
                    wait(Id, Nodes, Master, Refs, [{From, Ref}|Waiting])
            end;
        {ok, From, Ref} ->
            Refs2 = lists:delete({From, Ref}, Refs),
            wait(Id, Nodes, Master, Refs2, Waiting);
        release ->
            ok(Waiting),
            open(Id, Nodes)
   end.

ok(Waiting) ->
    lists:foreach(fun({F,R}) -> F ! {ok, self(), R} end, Waiting).

held(Id, Nodes, Waiting) ->
    receive
        {request, From, Ref, _} ->
            held(Id, Nodes, [{From, Ref}|Waiting]);
        release ->
            ok(Waiting),
            open(Id, Nodes)
    end.
