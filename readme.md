# Sistemas Distribuidos

## Trabajos practicos

Repositorio destinado a la resolucion de [trabajos practicos](https://gitlab.com/sistemas_distribuidos/curso) para la materia Sistemas Distrubuidos de la UNQ.

---
### Resoluciones

- [01-crash](https://gitlab.com/Maty11C/trabajos-practicos-syd/-/tree/master/01-crash)

- [04-rudy](https://gitlab.com/Maty11C/trabajos-practicos-syd/-/tree/master/04-rudy)

- [07-detector](https://gitlab.com/Maty11C/trabajos-practicos-syd/-/tree/master/07-detector)

- [16-opty](https://gitlab.com/Maty11C/trabajos-practicos-syd/-/tree/master/16-opty)

- [09-loggy](https://gitlab.com/Maty11C/trabajos-practicos-syd/-/tree/master/09-loggy)

- [12-muty](https://gitlab.com/Maty11C/trabajos-practicos-syd/-/tree/master/12-muty)

- [11-toty](https://gitlab.com/Maty11C/trabajos-practicos-syd/-/tree/master/11-toty)


---

### Trabajo final

- [ub-ar](https://gitlab.com/Maty11C/trabajos-practicos-syd/-/tree/master/ubar)

---