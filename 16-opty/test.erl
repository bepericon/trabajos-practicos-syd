-module(test).
-export([start/3]).

start(Server, N, Transactions) ->
    Clients = generate_clients(N),
    spawn_clients(Server, Clients),
    Start = erlang:system_time(micro_seconds),
    run_transactions(Clients, Transactions),
    Finish = erlang:system_time(micro_seconds),
    io:format("Para ~w clientes ejecutando ~w transacciones cada uno, tarda: ~w segundos ~n", [N, length(Transactions), (Finish - Start)/1000000]).

% Se generan N cantidad de clientes
generate_clients(N) ->
    Clients = [list_to_atom(lists:flatten(io_lib:format("Client~p", [erlang:unique_integer()]))) || _ <- lists:seq(1, N)],
    io:format("Clientes: ~n"),
    lists:foreach(fun(Client) -> io:format("~p~n", [Client]) end, Clients),
    Clients.

% Se establece la conexión con el servidor para cada cliente
spawn_clients(Server, Clients) ->
    lists:map(fun(NameClient) -> client:start(NameClient, Server, self()) end, Clients).

% Se envían concurrentemente las transacciones de N clientes y se espera hasta recibir la última respuesta
run_transactions(Clients, Transactions) ->
    send_transactions(Clients, Transactions),
    check_transactions(length(Clients) * length(Transactions)).

% Se ejecutan todas las transacciones por cada cliente
send_transactions(Clients, Transactions) ->
    lists:map(fun(Transaction) -> send_transaction(Clients, Transaction) end, Transactions).

% Se ejecuta la transacción por cada cliente
send_transaction(Clients, Transaction) ->
    lists:map(fun(Client) -> client:test(Transaction, Client, self()) end, Clients).

% Se chequean todas las transacciones enviadas
check_transactions(N) ->
    if
        N == 0 ->
            ok;
        true ->
            receive
                {termine, TypeResponse} ->
                    io:format("Checked transaction ~s~n", [TypeResponse]),
                    check_transactions(N - 1)
            end
    end.