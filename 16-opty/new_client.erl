-module(new_client).
-export([start/5, read/3, write/4, commit/2]).

start(NameClient, Server, Monitor, CountEntries, CountCommands) ->
    Client = spawn(fun() -> init(Server, Monitor) end),
    register(NameClient, Client),
    ListEntries = lists:seq(1, CountEntries),
    Commands = [write, read],
    generate_commands(NameClient, Server, Monitor, ListEntries, Commands, CountCommands).

generate_commands(NameClient, Server, Monitor, ListEntries, Commands, CountCommands) ->
    CurrentCommand = lists:nth(rand:uniform(length(Commands)), Commands),
    CurrentEntry = lists:nth(rand:uniform(length(ListEntries)), ListEntries),
    if
        CountCommands > 0 ->
            case CurrentCommand of
                write ->
                    Value = rand:uniform(1000),
                    write(NameClient, Monitor, CurrentEntry, Value);
                read ->
                    read(NameClient, Monitor, CurrentEntry)
            end,
            generate_commands(NameClient, Server, Monitor, ListEntries, Commands, CountCommands-1);
        true -> 
            commit(NameClient, Monitor),
            ok
    end.

init(Server, Monitor) ->
    Handler = server:open(Server),
    Ref = make_ref(),
    client(Handler, Ref, Monitor).

client(Handler, Ref, Monitor) ->
    receive
        {read, EntryNumber, Monitor} -> 
            Handler ! {read, Ref, EntryNumber},
            client(Handler, Ref, Monitor);
        {write, EntryNumber, Value, Monitor} -> 
            Handler ! {write, EntryNumber, Value},
            client(Handler, Ref, Monitor);
        {commit, Monitor} ->
            Handler ! {commit, Ref},
            client(Handler, Ref, Monitor);

        {read_response, Reference , Entry, Value, Time} -> 
            Monitor ! { read, {Reference , Entry, Value, Time}},
            client(Handler, Ref, Monitor);
        {_, ok} ->
            Monitor ! { commit_ok };
        {_, abort} ->
            Monitor ! { commit_abort }
    end.

read(NameClient, Monitor, N) ->
    NameClient ! {read, N, Monitor},
    ok.

write(NameClient, Monitor, N, Value) ->
    NameClient ! {write, N, Value, Monitor},
    ok.

commit(NameClient, Monitor) ->
    NameClient ! {commit, Monitor},
    ok.