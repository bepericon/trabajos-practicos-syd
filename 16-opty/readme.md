# Opty

Un servidor de transacciones utilizando control de concurrencia optimista.

[Enunciado](https://gitlab.com/sistemas_distribuidos/curso/-/blob/master/labs/16-opty.md)

## Inicializando

Es necesario cargar los siguientes módulos: client, entry, handler, server, store, validator.

Ejemplo:
```
eshell> c(client).
{ok,client}
...
```

A continuación, ejecutar la función start(N) del server:
```
eshell> server:start(2).
true
```

Tambien hay que iniciar el cliente, con su start(NameClient, NameHandler, Server) para establecer la conexión con el servidor:

```
eshell> client:start(client, handler, server).
true
```
Una vez establecida la conexión, se puede proceder a ejecutar las transacciones.

## Transacciones

### Read

Se lee el valor de determinado índice del store.
```
eshell> client:read(1, handler).
true
```

### Write

Se escribe un valor en determinado índice del store
```
eshell> client:read(1, handler).
true
```

### Commit

Se confirman las transacciones hechas previamente.
En caso de que la confirmación resulte exitosa se obtiene un ok, de lo contrario, si la validación detectó algún conflicto, se obtiene un abort.
```
eshell> client:read(1, handler).
true
```

## Test

Es necesario cargar el módulo test.
```
eshell> c(new_test).
{ok,client}
...
```

A continuación ejecutamos la función *start(Server, NEntries, NClients, NCommands)* del módulo new_test para verificar el comportamiento del server ante una determinada cantidad de clientes ejecutando cierta cantidad de operaciones aleatorias:
```
eshell> new_test:start(server, 20, 100000, 5).
Clientes creados -> cantidad: 100000 
Para 100000 clientes ejecutando 5 operaciones cada uno (read y write aleatorios) y cada cliente finalizando con un commit, se tarda: 1.008925 segundos 
 TRANSACTIONS: 100000 
 READS: 23115 
 COMMITS OK: 98841 
 COMMITS ABORT: 1159 
 HIT %: 98.84100000000001 
 ABORT %: 1.159
...
```

## Performance

- ¿Performa? ¿Cuántas transacciones podemos hacer por segundo? ¿Cuáles son las limitaciones en el número de transacciones concurrentes y la tasa de éxito? Esto por supuesto depende del tamaño del store, cuántas operaciones de write hace cada transacción, cuanto tiempo tenemos entre las instrucciones de read y el commit final. ¿Algo más?

Sí es una implementación que performe, comparado con los controles de concurrencia "pesimistas" es una método que mejora significativamente el rendimiento.
Sin embargo, creemos que es performante en determinados escenarios. En los casos donde las operaciones de lectura son más frecuentes que las de escritura, es decir, cuando el entorno frecuenta pocas operaciones de escritura, se comporta de manera eficiente ya que no necesita de resolver conflictos, no es más que informar al usuario el valor escrito en el store o en sus mismas escrituras en mejor caso. En cambio en un entorno en el que frecuente muchas operaciones de escritura, el costo de resolver de alguna forma las transacciones conflictivas es más alto, bajando la performance de la implementación del control de concurrencia.

Nuestro servidor permite que 100000 (cien mil) clientes puedan realizar a razon de 5 operaciones distintas, y finalizar su transaccion con un commit en aproximandamente 1 segundo. Pudimos obtener valores de aciertos y no, siendo la tasa de exito 98,8 % y la de error: 1,2 % aproximadamente.
Aumentando el numero de operaciones por cliente y manteniendo el resto de valores, estas tasas se tienden a igualar, rondando entre 53% de exito y 47% de error.

Las limitaciones en el número de transacciones respecto a la tasa de éxito son estrictamente la forma en que el control de concurrencia optimista resuelve los conflictos ante una situación en la que dos clientes operen sobre un mismo registro. La limitación es el bloqueo o "abort" de la transacción en cuanto se detecta que un commit entra en conflicto, por lo que el éxito o no de la transacción depende de si un cliente alteró el store (ejecutó el commit) antes que otro.
Creemos también que la tasa de éxito y las limitaciones en el número de transacciones va a depender también de, además de los motivos nombrados, del orden en el que el cliente haga sus operaciones de lectura. Esto significa que no es el mismo costo el de leer un registro no escrito en comparación al que fue escrito por el cliente con anterioridad; el registro no escrito necesita de buscarlo en el store mientras que el ya escrito está explícito en las operaciones de escritura.

- Algunas preguntas sobre Erlang también son interesantes plantear. ¿Es realista la implementación del store que tenemos? Independientemente del handler de transacciones, ¿qué rápido podemos operar sobre el store? ¿Qué sucede si hacemos esto en una red de Erlang distribuida, qué es lo que se copia cuando el handler de transacciones arranca? ¿Dónde corre el handler? ¿Cuáles son los pros y contras de la estrategia de
implementación?

No es un store realista porque en nuestra implementación el store guarda procesos. Una implementación realista debería permitir persistir la información que se quiera escribir, no solo manipular procesos

Independientemente del handler de transacciones, podremos operar en el store tan rápido como la estructura de datos que implemente lo permita. En nuestra implementación se utiliza una lista pero la eficiencia se vería mejorada si se utilizara una estructura de acceso más rápido, como por ejemplo una tabla de hash.