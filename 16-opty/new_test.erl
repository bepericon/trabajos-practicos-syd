-module(new_test).
-export([start/4]).

start(Server, NEntries, NClients, NCommands) ->
    Clients = generate_clients(NClients),
    Start = erlang:system_time(micro_seconds),
    Check = spawn(fun() -> check_transactions(NClients, NClients, NCommands, Start, {}) end),
    Monitor = monitor:start(Check),
    spawn_clients(Clients, Server, Monitor, NEntries, NCommands).

% Se generan N cantidad de clientes
generate_clients(N) ->
    Clients = [list_to_atom(lists:flatten(io_lib:format("Client~p", [erlang:unique_integer()]))) || _ <- lists:seq(1, N)],
    io:format("Clientes creados -> cantidad: ~w ~n", [length(Clients)]),
    Clients.

% Se establece la conexión con el servidor para cada cliente
spawn_clients(Clients, Server, Monitor, NEntries, NCommands) ->
    lists:map(fun(NameClient) -> new_client:start(NameClient, Server, Monitor, NEntries, NCommands) end, Clients).

% Se chequean todas las transacciones enviadas
check_transactions(Counter, NClients, NCommands, Start, Monitor) ->
    if
        Counter == 0 ->
            Finish = erlang:system_time(micro_seconds),
            io:format(
                "Para ~w clientes ejecutando ~w operaciones cada uno (read y write aleatorios) y cada cliente finalizando con un commit, se tarda: ~w segundos ~n", [NClients, NCommands, (Finish - Start)/1000000]),
            Monitor ! result,
            ok;
        true ->
            receive
                {monitor, PidMonitor} ->
                    check_transactions(Counter, NClients, NCommands, Start, PidMonitor);
                termine ->
                    check_transactions(Counter -1, NClients, NCommands, Start, Monitor)
            end
    end.