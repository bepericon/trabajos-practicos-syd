-module(logger_).
-export([start/1, stop/1]).

start(Nodes) ->
    spawn_link(fun() ->init(Nodes) end).

stop(Logger) ->
    Logger ! stop.

init(Nodes) ->
    Clock = time:clock(Nodes),
    Queue = [],
    loop(Clock, Queue, 0).

loop(Clock, Queue, MaxLen) ->
    receive
        {log, From, Time, Msg} ->
            NewClock = time:update(From, Time, Clock), % actualizar clock
            SortedQueue = lists:sort([{Time, From, Msg} | Queue]), % Se agrega el mensaje a la cola de retención y se ordena
            NewQueue = logger(NewClock, SortedQueue), % Se loggean los mensajes que son safe
            loop(NewClock, NewQueue, max(MaxLen, length(SortedQueue)));
         stop ->
            io:format("QUEUE MAX LENGTH: ~w ~n", [MaxLen]),
            ok
    end.

logger(Clock, Queue) ->
    lists:dropwhile(
        fun(QElem) ->
            {TimeNode, Node, Msg} = QElem,  
            case time:safe(TimeNode, Clock) of
                true ->
                    log(Node, TimeNode, Msg),
                    true;
                false -> false
            end
        end, 
        Queue).

log(From, Time, Msg) ->
    io:format("log: ~w ~w ~p~n", [Time, From, Msg]).
