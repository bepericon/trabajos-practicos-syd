-module(clockVector).
-export([start/1, update/3, safe/2]).

% Genera vectores inicializados en cero para cada nodo.
start(Nodes) ->
    lists:map(fun(Node) -> {Node, lists:duplicate(length(Nodes), 0)} end, Nodes).

% Actualiza el vector de determinado nodo
update(Clock, Node, Vector) ->
    lists:keyreplace(Node, 1, Clock, {Node, Vector}).

% Determina si el vector es menor a los vectores de los demas nodos
safe(Clock, Vector) ->
    lists:all(fun({_, CurrentVector}) -> leq(Vector, CurrentVector) end, Clock).

% Determina si un vector es menor que otro
leq(V1, V2) ->
    lists:all(fun(X) -> X end, (lists:zipwith(fun(X, Y) -> X =< Y end, V1, V2))).