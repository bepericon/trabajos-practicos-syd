-module(time).
-export([zero/0, inc/2, merge/2, leq/2, clock/1, update/3, safe/2]).

zero() -> 0.

inc(_, T) -> T + 1.

merge(Ti, Tj) -> max(Ti, Tj).

leq(Ti, Tj) -> Ti < Tj.

clock(Nodes) -> clock:start(Nodes).

update(Node, Time, Clock) -> clock:update(Clock, Node, Time).

safe(Time, Clock) -> clock:safe(Clock, Time).
